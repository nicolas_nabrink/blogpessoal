from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime, date

class Category(models.Model):
    title = models.CharField(max_length=255)
    body  = models.TextField()

    def __str__(self):
        return self.title

class Post(models.Model):
    categories = models.ManyToManyField(Category, blank=False, related_name="posts")
    title = models.CharField(max_length=255)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    imageURL = models.URLField(max_length=300)
    datetime = models.DateTimeField(auto_now_add=True)
    body  = models.TextField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})

class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    body  = models.TextField()

    def __str__(self):
        return '%s - %s' %(self.post.title, self.author)




