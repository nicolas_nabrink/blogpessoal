from django import forms
from .models import Post, Comment, Category

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('categories','title', 'author', 'imageURL', 'body')

        widgets = {
            'categories': forms.CheckboxSelectMultiple(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'O título da sua postagem', 'label': 'alo'}),
            'author': forms.Select(attrs={'class': 'form-control'}),
            'imageURL': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'Link de uma imagem associada à postagem'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'O conteúdo da postagem'}),
        }

class EditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('categories','title', 'imageURL', 'body')

        widgets = {
            'categories': forms.CheckboxSelectMultiple(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'O título da sua postagem', 'label': 'alo'}),
            'imageURL': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'Link de uma imagem associada à postagem'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'O conteúdo da postagem'}),
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'body')

        widgets = {
            'author': forms.Select(attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'O conteúdo do comentário'}),
        }