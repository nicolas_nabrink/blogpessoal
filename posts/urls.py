from django.urls import path
from .views import Index, List, Detail, Create, Update, Delete, Comment, Categories, Category
from . import views

urlpatterns = [
    path('list/', List.as_view(), name='list'),
    path('', Index.as_view(), name='index'),
    path('list/search/', views.search_posts, name='search'),
    path('list/create/', Create.as_view(), name='create'),
    path('list/<int:pk>/', Detail.as_view(), name='post-detail'),
    path('list/update/<int:pk>/', Update.as_view(), name='update'),
    path('list/delete/<int:pk>/', Delete.as_view(), name='delete'),
    path('list/<int:pk>/comment', Comment.as_view(), name="comment"),
    path('categories/', Categories.as_view(), name='categories'),
    path('categories/<int:pk>/', Category.as_view(), name='category-detail'),
]