from django.shortcuts import render
from .models import Post, Comment, Category
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from .forms import PostForm, EditForm, CommentForm
from django.urls import reverse_lazy

class Index(ListView):
    model = Post
    template_name = 'posts/index.html'

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(title__icontains=search_term)
        context = {"post_list": post_list}
    return render(request, 'posts/search.html', context)

class List(ListView):
    model = Post
    template_name = 'posts/list.html'
    ordering = ['-datetime'] 

class Detail(DetailView):
    model = Post
    template_name = 'posts/detail.html'

class Create(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'posts/create.html'

class Update(UpdateView):
    model = Post
    form_class = EditForm
    template_name = 'posts/update.html'

class Delete(DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    success_url = reverse_lazy('list')

class Comment(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'posts/comment.html'

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse_lazy('post-detail', kwargs={'pk': self.kwargs['pk']})

class Categories(ListView):
    model = Category
    template_name = 'posts/categories.html'

class Category(DetailView):
    model = Category
    template_name = 'posts/category.html'